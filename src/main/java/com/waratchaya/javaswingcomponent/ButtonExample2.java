/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.javaswingcomponent;

import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Melon
 */
public class ButtonExample2 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Button Example");
        final JTextField textf = new JTextField();

        textf.setBounds(50, 50, 150, 20);

        JButton button = new JButton("Click Here");
        button.setBounds(50, 100, 95, 30);
        button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                textf.setText("Welcome to Java ^^");
            }
        });
        
        frame.add(button);
        frame.add(textf);
        frame.setSize(300,300);
        frame.setLayout(null);
        frame.setVisible(true);

    }
}
