/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.javaswingcomponent;

import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Melon
 */
public class JPasswordFieldExample2 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        final JLabel label = new JLabel();
        label.setBounds(20, 150, 200, 50);

        final JPasswordField pass = new JPasswordField();
        pass.setBounds(100, 75, 100, 30);

        JLabel la1 = new JLabel("Username:");
        la1.setBounds(20, 20, 80, 30);

        JLabel la2 = new JLabel("Password");
        la2.setBounds(20, 75, 80, 30);

        JButton button = new JButton("Login");
        button.setBounds(100, 120, 80, 30);

        final JTextField tx = new JTextField();
        tx.setBounds(100, 20, 100, 30);

        frame.add(pass);
        frame.add(la1);
        frame.add(label);
        frame.add(la2);
        frame.add(button);
        frame.add(tx);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);

        button.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {       
                   String data = "Username " + tx.getText();  
                   data += ", Password: "   
                   + new String(pass.getPassword());   
                   label.setText(data);          
                }  
             });   
        }
}
        
