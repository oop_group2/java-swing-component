/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.javaswingcomponent;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author Melon
 */
public class ColorChooserExample2 extends JFrame implements ActionListener {

    JFrame frame;
    JButton button;
    JTextArea ta;

    ColorChooserExample2() {
        frame = new JFrame("Color Chooser Example.");
        button = new JButton("Pad Color");
        button.setBounds(200, 250, 100, 30);
        ta = new JTextArea();
        ta.setBounds(10, 10, 300, 200);
        button.addActionListener(this);
        frame.add(button);
        frame.add(ta);
        frame.setLayout(null);
        frame.setSize(400, 400);
        frame.setVisible(true);
    }

    public void actionPerformed(ActionEvent e) {
        Color c = JColorChooser.showDialog(this, "Choose", Color.CYAN);
        ta.setBackground(c);
    }

    public static void main(String[] args) {
        new ColorChooserExample2();
    }
}
