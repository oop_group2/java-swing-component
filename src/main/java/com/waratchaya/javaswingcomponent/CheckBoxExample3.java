/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.javaswingcomponent;

import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Melon
 */
public class CheckBoxExample3 extends JFrame implements ActionListener {

    JLabel label;
    JCheckBox cb1, cb2, cb3;
    JButton button;

    CheckBoxExample3() {
        label = new JLabel("Food Ordering System");
        label.setBounds(50, 50, 300, 20);

        cb1 = new JCheckBox("Cake @ 100");
        cb1.setBounds(100, 100, 150, 20);

        cb2 = new JCheckBox("Moji @ 30");
        cb2.setBounds(100, 150, 150, 20);

        cb3 = new JCheckBox("Tea @ 10");
        cb3.setBounds(100, 200, 150, 20);

        button = new JButton("Order");
        button.setBounds(100, 250, 80, 30);
        button.addActionListener(this);
        
        add(label);
        add(cb1);
        add(cb2);
        add(cb3);
        add(button);  
        setSize(400,400);  
        setLayout(null);  
        setVisible(true);  
        setDefaultCloseOperation(EXIT_ON_CLOSE);  
    }
    public void actionPerformed(ActionEvent e){  
        float amount=0;  
        String msg="";  
        if(cb1.isSelected()){  
            amount+=100;  
            msg="Pizza: 100\n";  
        }  
        if(cb2.isSelected()){  
            amount+=30;  
            msg+="Burger: 30\n";  
        }  
        if(cb3.isSelected()){  
            amount+=10;  
            msg+="Tea: 10\n";  
        }  
        msg+="-----------------\n";  
        JOptionPane.showMessageDialog(this,msg+"Total: "+amount);  
    }  
    

    public static void main(String[] args) {
        new CheckBoxExample3();
    }
}
