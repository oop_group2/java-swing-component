/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.javaswingcomponent;

import javax.swing.*;

/**
 *
 * @author Melon
 */
public class CheckBoxExample1 {
    CheckBoxExample1(){
        JFrame frame = new JFrame("CheckBox Exmple");
        JCheckBox checkbox1 = new JCheckBox("C++");
        checkbox1.setBounds(100,100, 100,50);  
        JCheckBox checkbox2 = new JCheckBox("Java", true);  
        checkbox2.setBounds(100,150, 100,50);  
        frame.add(checkbox1);
        frame.add(checkbox2);
        frame.setSize(400,400);
        frame.setLayout(null);
        frame.setVisible(true);
    }
    public static void main(String[] args) {
        new CheckBoxExample1();
    }
}
