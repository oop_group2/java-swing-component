/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.javaswingcomponent;


import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Melon
 */
public class OptionPaneExample4 extends WindowAdapter{
    JFrame frame;  
OptionPaneExample4(){  
    frame = new JFrame();   
    frame.addWindowListener(this);
    frame.setSize(300, 300);  
    frame.setLayout(null);  
    frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);  
    frame.setVisible(true);  
}  
public void windowClosing(WindowEvent e) {  
    int a=JOptionPane.showConfirmDialog(frame,"Are you sure?");  
if(a==JOptionPane.YES_OPTION){  
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
}  
}  
public static void main(String[] args) {  
    new  OptionPaneExample4();  
}     
}
