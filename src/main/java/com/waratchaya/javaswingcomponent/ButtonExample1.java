/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.javaswingcomponent;

import javax.swing.*;

/**
 *
 * @author Melon
 */
public class ButtonExample1 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Button Example");

        JButton button = new JButton("Click Here");

        button.setBounds(100, 100, 95, 30);
        frame.add(button);
        frame.setSize(300, 300);
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
