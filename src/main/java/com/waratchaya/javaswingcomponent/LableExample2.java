/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.javaswingcomponent;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 *
 * @author Melon
 */
public class LableExample2 extends Frame implements ActionListener {

    JTextField textf;
    JLabel la;
    JButton button;

    LableExample2() {
        textf = new JTextField();
        textf.setBounds(50, 50, 150, 20);

        la = new JLabel();
        la.setBounds(50, 100, 250, 20);

        button = new JButton("Find IP");
        button.setBounds(50, 150, 95, 30);
        button.addActionListener(this);
        add(button);
        add(textf);
        add(la);
        setSize(400,400);
        setLayout(null);
        setVisible(true);

    }

    public void actionPerformed(ActionEvent e) {
        try{
            String host = textf.getText();  
        String ip=java.net.InetAddress.getByName(host).getHostAddress();  
        la.setText("IP of "+host+" is: "+ip);  
        }catch(Exception ex){System.out.println(ex);}  
    }  

    public static void main(String[] args) {
        new LableExample2();
    }

}
