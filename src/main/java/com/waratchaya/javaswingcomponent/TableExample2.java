/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.javaswingcomponent;

import javax.swing.*;
import javax.swing.event.*;

/**
 *
 * @author Melon
 */
public class TableExample2 {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Table Example");
        String data[][] = {{"102", "Mint", "670000"},
        {"202", "Git", "780000"},
        {"305", "Milk", "700000"}};
        String column[] = {"ID", "NAME", "SALARY"};
        final JTable jt = new JTable(data, column);
        jt.setCellSelectionEnabled(true);
        ListSelectionModel select = jt.getSelectionModel();
        select.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        select.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                String Data = null;
                int[] row = jt.getSelectedRows();
                int[] columns = jt.getSelectedColumns();
                for (int i = 0; i < row.length; i++) {
                    for (int j = 0; j < columns.length; j++) {
                        Data = (String) jt.getValueAt(row[i], columns[j]);
                    }
                }
                System.out.println("Table element selected is: " + Data);
            }
    });
         JScrollPane sp=new JScrollPane(jt);    
            frame.add(sp);  
            frame.setSize(300, 200);  
            frame.setVisible(true);  
    }          
}
