/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waratchaya.javaswingcomponent;

import javax.swing.*;

/**
 *
 * @author Melon
 */
public class TableExample1 {

    JFrame frame;

    TableExample1() {
        frame = new JFrame();
        String data[][] = {{"102", "Mint", "670000"},
        {"202", "Git", "780000"},
        {"305", "Milk", "700000"}};
        String column[] = {"ID", "NAME", "SALARY"};
        JTable jt = new JTable(data, column);
        jt.setBounds(30, 40, 200, 300);
        JScrollPane sp = new JScrollPane(jt);
        frame.add(sp);
        frame.setSize(300, 400);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        new TableExample1();
    }
}
